import gulp from "gulp";
const { src, dest, watch, series, parallel } = gulp;

import bsc from "browser-sync";
const browserSync = bsc.create();

import clean from "gulp-clean";
import * as dartSass from "sass";
import gulpSass from "gulp-sass";
import imagemin from "gulp-imagemin";
import autoprefixer from "gulp-autoprefixer";
import csso from "gulp-csso";

const sass = gulpSass(dartSass);

const htmlTaskHandler = () => {
  return src("./src/*.html").pipe(dest("./dist"));
};

const cssTaskHandler = () => {
  return src("./src/scss/main.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer())
    .pipe(csso())
    .pipe(dest("./dist/css"))
    .pipe(browserSync.stream());
};

const imagesTaskHandler = () => {
  return src("./src/img//*.*").pipe(imagemin()).pipe(dest("./dist/img"));
};

const fontTaskHandler = () => {
  return src("./src/structure//*.*").pipe(dest("./dist/structure"));
};

const cleanDistTaskHandler = () => {
  return src("./dist", { read: false, allowEmpty: true }).pipe(
    clean({ force: true })
  );
};

const browserSyncTaskHandler = () => {
  browserSync.init({
    server: {
      baseDir: "./dist",
    },
  });

  watch("./src/scss//*.scss").on(
    "all",
    series(cssTaskHandler, browserSync.reload)
  );
  watch("./src/*.html").on(
    "change",
    series(htmlTaskHandler, browserSync.reload)
  );
  watch("./src/img//*").on(
    "all",
    series(imagesTaskHandler, browserSync.reload)
  );
};

const cleaning = cleanDistTaskHandler;
const html = htmlTaskHandler;
const css = cssTaskHandler;
const font = fontTaskHandler;
const images = imagesTaskHandler;

export const build = series(
  cleanDistTaskHandler,
  parallel(htmlTaskHandler, cssTaskHandler, fontTaskHandler, imagesTaskHandler)
);
export const dev = series(build, browserSyncTaskHandler);